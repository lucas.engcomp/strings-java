public class FunctionsStrings {
    public static void main(String[] args) {
        String origin = "-LUCAS, engenharia, ENGENHARIA AStroFISica     -";

        String s1 = origin.toLowerCase();
        String s2 = origin.toUpperCase();
        String s3 = origin.trim();
        String s4 = origin.substring(2);
        String s5 = origin.substring(2, 8);
        String s6 = origin.replace('e', 'I');
        String s7 = origin.replace("engenharia", "engineering");
        int i = origin.indexOf("ia");
        int j = origin.lastIndexOf("Sic");

        System.out.println("Origin: " + origin);
        System.out.println("ToLowerCase " + s1);
        System.out.println("ToUpperCase " + s2);
        System.out.println("Trim " + s3);
        System.out.println("Substring 2 casas " + s4);
        System.out.println("Substring iniciando em 2 até 8 casas " + s5);
        System.out.println("Replace " + s6);
        System.out.println("Replace especifico " + s7);
        System.out.println("IndexOf " + i);
        System.out.println("LastIndexOf " + j);



    }
}
